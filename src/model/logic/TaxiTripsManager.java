package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Servicio;

public class TaxiTripsManager implements ITaxiTripsManager {

	private IStack<Servicio> stack = new Stack<Servicio>();

	private IQueue<Servicio> queue = new Queue<Servicio>();

	public void loadServices(String serviceFile, String taxiId)
	{
		System.out.println("Inside loadServices with File:" + serviceFile);
		System.out.println("Inside loadServices with TaxiId:" + taxiId);

		JsonParser parser = new JsonParser();
		String id, time;
		long inicio = -1;

		try
		{
			JsonArray arr= (JsonArray) parser.parse(new FileReader(serviceFile));

			for(int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);

				if(obj.get("taxi_id") != null && obj.get("taxi_id").getAsString().equals(taxiId))
				{
					System.out.println(obj);
					id = (obj.get("trip_id") != null) ? (obj.get("trip_id").getAsString()) : null; 

					if(obj.get("trip_start_timestamp") != null)
					{
						time = obj.get("trip_start_timestamp").getAsString();
						SimpleDateFormat formato = new SimpleDateFormat("YYYY-MM-DD'T'HH:mm:ss'.'000");
						try
						{
							inicio = formato.parse(time).getTime();
						}
						catch(ParseException e)
						{
							// NO va a pasar, joven.
						}
					}

					Servicio servicio = new Servicio(id, inicio);

					stack.push(servicio);
					queue.enqueue(servicio);
				}
			}
		}
		catch (JsonIOException e1 ) {
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			e3.printStackTrace();
		}
	}

	@Override
	public int [] servicesInInverseOrder() 
	{
		System.out.println("Inside servicesInInverseOrder");
		int [] resultado = new int[2];
		resultado[0] = 0;
		resultado[1] = 0;

		if(!stack.isEmpty())
		{
			IStack<Servicio> respaldo = new Stack<Servicio>();
			Servicio respaldop = stack.pop();
			respaldo.push(respaldop);
			long primero = respaldop.inicio();
			resultado[0] = 1;
 			long segundo;

			while(!stack.isEmpty())
			{
				respaldop = stack.pop();
				respaldo.push(respaldop);
				segundo = respaldop.inicio();
				if(primero >= segundo)
				{
					resultado[0] = resultado[0] + 1;
					primero = segundo;
				}
				else
				{
					resultado[1] = resultado[1] + 1;
				}
			}
			
			while(!respaldo.isEmpty())
			{
				stack.push(respaldo.pop());
			}
		}
		return resultado;
	}

	@Override
	public int [] servicesInOrder() 
	{
		System.out.println("Inside servicesInOrder");
		int [] resultado = new int[2];
		resultado[0] = 0;
		resultado[1] = 0;

		if(!queue.isEmpty())
		{
			IQueue<Servicio> respaldo = new Queue<Servicio>();
			Servicio respaldop = queue.dequeue();
			respaldo.enqueue(respaldop);
			long primero = respaldop.inicio();
			resultado[0] = 1;
 			long segundo;

			while(!queue.isEmpty())
			{
				respaldop = queue.dequeue();
				respaldo.enqueue(respaldop);
				segundo = respaldop.inicio();
				if(primero <= segundo)
				{
					resultado[0] = resultado[0] + 1;
					primero = segundo;
				}
				else
				{
					resultado[1] = resultado[1] + 1;
				}
			}
			queue = respaldo;
		}
		return resultado;
	}


}
