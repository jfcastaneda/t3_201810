package model.vo;

/**
 * Representation of a Service object
 */
public class Servicio
{	
	
	/**
	 * Id del servicio.
	 */
	private String id;
	
	/**
	 * Momento en que empez� el viaje.
	 */
	private long inicio;
	
	/**
	 * M�todo constructor del servicio.
	 */
	public Servicio(String id, long inicio)
	{
		this.id = id;
		this.inicio = inicio;
	}
	
	/**
	 * M�todo que retorna el tripId.
	 */
	public String id()
	{
		return id;
	}
	
	/**
	 * M�todo que retorna el tiempo de inicio.
	 */
	public long inicio()
	{
		return inicio;
	}
	
}
