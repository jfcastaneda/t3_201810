package model.data_structures;

public class Queue<E> implements IQueue<E>
{
	private Lista<E> list;
	
	/**
	 * Constructor.
	 */
	public Queue()
	{
		list = new Lista<E>();
	}
	
	/** Enqueue a new element at the end of the queue */
	public void enqueue(E item)
	{
		list.agregarAlFinal(item);
	}
	
	/** Dequeue the "first" element in the queue
	 * @return "first" element or null if it doesn't exist
	 */
	public E dequeue()
	{
		return list.quitarPrimero();
	}
	
	/** Evaluate if the queue is empty. 
	 * @return true if the queue is empty. false in other case.
	 */
	public boolean isEmpty()
	{
		return list.isEmpty();
	}
}
