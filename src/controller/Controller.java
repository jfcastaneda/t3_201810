package controller;

import api.ITaxiTripsManager;
import model.logic.TaxiTripsManager;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	private static ITaxiTripsManager  manager = new TaxiTripsManager();
	
	/** To load the services of the taxi with taxiId */
	public static void loadServices( String taxiId ) {
		// To define the dataset file's name 
		String serviceFile = "./data/taxi-trips-wrvz-psew-subset-medium.json";
		
		manager.loadServices( serviceFile, taxiId );
	}
		
	public static int [] servicesInInverseOrder() {
		return manager.servicesInInverseOrder();
	}
	
	public static int [] servicesInOrder() {
		return manager.servicesInOrder();
	}
}
