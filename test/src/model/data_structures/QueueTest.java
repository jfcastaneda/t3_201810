package model.data_structures;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class QueueTest 
{
	private IQueue<Integer> queue;
	
	@Before
	public void setupEscenario()
	{
		queue = new Queue<Integer>();
	}
	
	@Test
	public void testQueue()
	{
		assertTrue("No funciona isEmpty", queue.isEmpty());
		
		queue.enqueue(1);
		assertTrue("No funciona enqueue", !queue.isEmpty());
		
		Integer oli = queue.dequeue();
		assertTrue("No funciona dequeue", oli == 1);
	}
}
