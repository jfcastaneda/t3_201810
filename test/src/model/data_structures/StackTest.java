package model.data_structures;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class StackTest 
{
	private IStack<Integer> stack;

	@Before
	public void setupEscenario()
	{
		stack = new Stack<Integer>();
	}

	@Test
	public void testQueue()
	{
		assertTrue("No funciona isEmpty", stack.isEmpty());

		stack.push(1);
		assertTrue("No funciona push", !stack.isEmpty());

		Integer oli = stack.pop();
		assertTrue("No funciona pop", oli == 1);
	}
}
